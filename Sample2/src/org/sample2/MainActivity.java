package org.sample2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
    /** Called when the activity is first created. */
	private TextView mDateDisplay;
    private Button mPickDate;
    Context context = this;
    private int mYear;
    private int mMonth;
    private int mDay;
    private String year;
    private String month;
    private String day;
    private String YTD_Cur;
//    private ArrayList<JsonNewsItem> m_orders = new ArrayList<JsonNewsItem>();
    private ArrayList<String> arraylist = new ArrayList<String>();
    static final int DATE_DIALOG_ID = 0;

    private Button yesterday;
    private Button tomorrow;
    private String date;
    Date mdate;
    
    static int count =0;
//    private ArrayAdapter<JsonNewsItem> adapter;
    private ArrayAdapter<String> adapter;
    private ListView list;

    Calendar calendar = new GregorianCalendar(Locale.KOREA);
    @SuppressLint("NewApi")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(android.os.Build.VERSION.SDK_INT > 9) {

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

                StrictMode.setThreadPolicy(policy);

        }
        // capture our View elements
        yesterday = (Button) findViewById(R.id.yesterday);
        tomorrow = (Button) findViewById(R.id.tomorrow);
        
        mDateDisplay = (TextView) findViewById(R.id.dateDisplay);
        mPickDate = (Button) findViewById(R.id.pickDate);
        list = (ListView) findViewById(R.id.list);

        tomorrow.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				calendar.add(Calendar.DATE, 1);
		        year = String.valueOf(calendar.get(Calendar.YEAR));
		        month = String.valueOf(calendar.get(Calendar.MONTH)+1);
		        day = String.valueOf(calendar.get(Calendar.DATE));
		        mYear   =	calendar.get(Calendar.YEAR);
		        mMonth  =	calendar.get(Calendar.MONTH)+1;
		        mDay 	=	calendar.get(Calendar.DATE);
		        if(month.length()<2)
		        	month = "0" + month;
		        if(day.length()<2)
		        	day = "0" + day;
		        date = year+month+day;
		        if(date.compareTo(YTD_Cur) > 0)
		        		Toast.makeText(context, "기록 정보가 없습니다.", 500).show();
		        else{

		        mDateDisplay.setText(date);
		        remove();
		        TimeLineMaker(date);
		        }

			}
        	
        });
        
        
 //       String cur = CurDateFormat.format(tomorrow);

        yesterday.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				calendar.add(Calendar.DATE, -1);
				year = String.valueOf(calendar.get(Calendar.YEAR));
		        month = String.valueOf(calendar.get(Calendar.MONTH)+1);
		        day = String.valueOf(calendar.get(Calendar.DATE));
		        mYear   =	calendar.get(Calendar.YEAR);
		        mMonth  =	calendar.get(Calendar.MONTH)+1;
		        mDay 	=	calendar.get(Calendar.DATE);
	/*	        year = String.valueOf(mYear);
		        month= String.valueOf(mMonth);
		        day  = String.valueOf(mDay);
	*/	        if(month.length()<2)
		        	month = "0" + month;
		        if(day.length()<2)
		        	day = "0" + day;
		        date = year+month+day;
		        mDateDisplay.setText(date);
		        remove();
		        TimeLineMaker(date);

			}
        	
        });
 

 //       adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1  , arraylist);
 //       		m_orders);

        // display the current date
        
        // add a click listener to the button
        mPickDate.setOnClickListener(new OnClickListener() {
            @SuppressWarnings("deprecation")
			public void onClick(View v) {
                showDialog(DATE_DIALOG_ID);
            }
        });
        calendar.add(Calendar.DATE, -1);
        year = String.valueOf(calendar.get(Calendar.YEAR));
        month = String.valueOf(calendar.get(Calendar.MONTH)+1);
        day = String.valueOf(calendar.get(Calendar.DATE));
        mYear   =	calendar.get(Calendar.YEAR);
        mMonth  =	calendar.get(Calendar.MONTH)+1;
        mDay 	=	calendar.get(Calendar.DATE);

        
        
        if(month.length()<2)
        	month = "0" + month;
        if(day.length()<2)
        	day = "0" + day;
        date = year+month+day;
        YTD_Cur = date;
        Toast.makeText(context, YTD_Cur, 500).show();
        mDateDisplay.setText(date);
//        adapter = new ArrayAdapter<JsonNewsItem>(context, R.layout.listitem, TimeLineMaker(date));
        adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, arraylist);
		list.setAdapter(adapter);
 //       TimeLineMaker(date);
        
     // capture our View elements
 
        // add a click listener to the button

       

        // display the current date

    }
   
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        case DATE_DIALOG_ID:
            return new DatePickerDialog(this,
                        mDateSetListener,
                        mYear, mMonth-1, mDay);
        }
        return null;
    }
   
    // updates the date we display in the TextView
    private void updateDateDisplay() {
    	if(month.length()<2)
    		month = "0" + month;
    	if(day.length()<2)
    		day = "0" + day;
        date = year+month+day;
        if(date.compareTo(YTD_Cur) > 0){
    		Toast.makeText(context, "기록 정보가 없습니다.", 500).show();
    		calendar.add(Calendar.DATE, -1);
        }
        else{
        	mDateDisplay.setText(
                    new StringBuilder()
                            // Month is 0 based so add 1
                    		.append(year)
                            .append(month)
                            .append(day)
                            );
        	calendar.set(mYear, mMonth, mDay);


        	remove();
        	TimeLineMaker(date);
        }

    }
   
    // updates the time we display in the TextView
   private void remove(){
	   /*
	   if(!m_orders.isEmpty())
       	for(int i =0; i < m_orders.size() ; i++)
       		m_orders.removeAll(m_orders);
	   */
	   if(!arraylist.isEmpty())
	       	for(int i =0; i < arraylist.size() ; i++)
	       		arraylist.removeAll(arraylist);
	   adapter.notifyDataSetChanged();
   }
    // the callback received when the user "sets" the date in the dialog
    private DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int sub_year,
                                      int monthOfYear, int dayOfMonth) {
                    mYear = sub_year;
                    year = String.valueOf(sub_year);
                    mMonth = monthOfYear;
                    month = String.valueOf(monthOfYear+1);
                    mDay = dayOfMonth;
                    day = String.valueOf(dayOfMonth);
                    
                    updateDateDisplay();
                }
            };
           
         // the callback received when the user "sets" the time in the dialog

     private ArrayList<String> TimeLineMaker(String date){
    	 //new TimeLineThread(date).execute(null,null,null);
    	String key = "";
    	String news = "";
    	String url_info = "";
        String line;
 		String page ="";
 		String group1 = "http://kr.core.today/json/?n=10";
 		String time1 = "&d=";
        String sample = group1 + time1 + date;
         
         URL url;
         HttpURLConnection urlConnection;
         BufferedReader bufreader;
         try {

 		url = new URL(sample);
 		
 		urlConnection = (HttpURLConnection) url.openConnection();

 		bufreader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(),"UTF-8"));
 		while((line=bufreader.readLine())!=null){
 			page+=line;
 		}

 		JSONObject test;
 		JSONArray alljson = new JSONArray(page); 
 		for(int i =0 ; i <10 ; i++){
 		test=new JSONObject(alljson.getString(i));
 		key = test.getString("k");
// 		news = test.getString("s");
//  		url_info = test.getString("i");
// 		m_orders.add(new JsonNewsItem(key,news,url_info));
 		arraylist.add(key);
 //		arraylist.add(key);
 		}
 		adapter.notifyDataSetChanged();
 		urlConnection.disconnect();
 		
 		}
 		catch(Exception e){
 			remove();
// 			m_orders.add(new JsonNewsItem("기사가 없습니다.","a","b"));
 			arraylist.add("기사가 없습니다");
 		}
         return arraylist;
 //        	return m_orders;
     }
     
     public class NewsViewHolder{
     	TextView tt;
     	TextView bt;
     	TextView ct;
     	boolean isOpen;
     }
     
     /////////////////////////////////////////////////////////////////////////////////////////
 /*    public class JsonNewsAdapter extends ArrayAdapter<JsonNewsItem> {
 		private ArrayList<JsonNewsItem> items;

 		public JsonNewsAdapter(Context context, int textViewResourceId, ArrayList<JsonNewsItem> items) {
 			super(context, textViewResourceId, items);
 			this.items = items;
 		}
 	    
 		@Override
 		public View getView(int position, View convertView, ViewGroup parent) {    
 			int count;
 			View v = convertView;  
 			NewsViewHolder holder = null;
 			//v.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, 40));
 			if (v == null) {      
 				LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);     
 	                	v = vi.inflate(R.layout.listitem, null);
 	                	holder = new NewsViewHolder();
 	                	holder.tt= (TextView) v.findViewById(R.id.dataItem01);
 	                	holder.bt= (TextView) v.findViewById(R.id.dataItem02);
 	                	holder.ct= (TextView) v.findViewById(R.id.dataItem03);
 	                	holder.isOpen=false;
 	                	
 			}
 			else
 			{
 				holder=(NewsViewHolder)v.getTag();
 			}
 			JsonNewsItem p = items.get(position);     
 	        	/*if (p != null) {                 
 			TextView tt = (TextView) v.findViewById(R.id.dataItem01);        
 			TextView bt = (TextView) v.findViewById(R.id.dataItem02);
 			TextView ct = (TextView) v.findViewById(R.id.dataItem03);
 			View toolbar = v.findViewById(R.id.toolbar);
 			
 			if(position!=isOpenindex)
 				toolbar.setVisibility(View.GONE);
 			
 			if (tt != null){                 
 				tt.setText(p.getTitle());                                   
 			}
 			if(bt != null){                  
 				bt.setText(p.getLink());             
 			}
 			if(ct != null){                  
 				ct.setText(p.getDescription());      
 			}
 			
 		}    */
 /*			v.setTag(holder);
 			View toolbar = v.findViewById(R.id.toolbar);
 			if(holder.isOpen){
 				toolbar.setVisibility(View.GONE);
 				if(p.isOpen){
 					toolbar.setVisibility(View.VISIBLE);
 					Toast.makeText(context, "isOpen", 500).show();
 				}
 				
 			}
 			
 			if(p.isOpen){
 				toolbar.setVisibility(View.VISIBLE);
 				Toast.makeText(context, "isOpen", 500).show();
 			}
 			else{
 				toolbar.setVisibility(View.GONE);
 			}
 			
 			if(p!=null){
 				holder.tt.setText(p.getTitle());
 				holder.bt.setText(p.getLink());
 				holder.ct.setText(p.getDescription());
 				holder.isOpen=p.getisOpen();
 			}
 		return v;   
 		} 
 	}
 */    ///////////////////////////////////////////////////////////////////////listview adapter
     /*
     private class TimeLineThread extends AsyncTask<Void, Void, Void>{
    	 String cDate;
    	 protected TimeLineThread(String date){
    		 cDate = date;
    	 }
    	 @Override
    	 protected Void doInBackground(Void... params){
    		 	String key = "";
    	        String line;
    			String page ="";
    			String group1 = "http://kr.core.today/json/?n=10";
    			String time1 = "&d=";
    	        String sample = group1 + time1 + cDate;
    	        
    	        URL url;
    	        HttpURLConnection urlConnection;
    	        BufferedReader bufreader;
    	        try {

    			url = new URL(sample);
    			
    			urlConnection = (HttpURLConnection) url.openConnection();

    			bufreader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(),"UTF-8"));
    			while((line=bufreader.readLine())!=null){
    				page+=line;
    			}
    	
    			JSONObject test;
    			JSONArray alljson = new JSONArray(page); 
    			for(int i =0 ; i <10 ; i++){
    			test=new JSONObject(alljson.getString(i));
    			key = test.getString("k");
    			arraylist.add(key);

    			}
    			adapter.notifyDataSetChanged();
    			urlConnection.disconnect();  			
    			}
    			catch(Exception e){
    				Toast.makeText(getBaseContext(), e.toString(), Toast.LENGTH_SHORT).show();
    			}
				return null;
    	 }
     }
     */
}